import React, {Component} from 'react';
import PropTypes from 'prop-types';
const ModalButton = ({text, backgroundColor,onClick})=> {
    return (
        <button className="modal_button" style={{backgroundColor}} onClick={onClick}></button>
    );
    ModalButton.propTypes ={
        text: PropTypes.string,
        backgroundColor: PropTypes.string,
        onClick: PropTypes.func
    }
}
export default ModalButton;