import React, {Component} from 'react';
import PropTypes from 'prop-types';
const ModalWindow =({header,CloseIcon, movement, text,CloseButton})=> {
    return (
        <div className="modal_Window">
            <header>
                {header}
                {CloseIcon && <button onClick={CloseButton} className="CloseButton">X</button>}
            </header>
            <p className="modal_Text">{text}</p>
            {movement}
        </div>
    );
}
export default ModalWindow;